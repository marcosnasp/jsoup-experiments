package org.example;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Document.OutputSettings.Syntax;
import org.jsoup.safety.Whitelist;

public class ParseHtmlHelp {

	public void parse(String texto) {
		
		String docNone = Jsoup.clean(texto, Whitelist.none());
		Document docNoneDocument = Jsoup.parse(docNone);
		docNone = docNoneDocument.toString();
		docNone = Jsoup.parse(docNone).toString();
		
		System.out.println("\n---------WHITELIST - NONE-----------------");
		System.out.println(docNone);
		System.out.println("---------------------------\n");
		
		System.out.println("\n---------WHITELIST - RELAXED-----------------");
		String docRelaxed = Jsoup.clean(texto, Whitelist.relaxed());
		System.out.println(docRelaxed);
		System.out.println("---------------------------\n");
		
		System.out.println("\n---------WHITELIST - BASICWITHIMAGES-----------------");
		String docBasicWithImages = Jsoup.clean(texto, Whitelist.basicWithImages());
		System.out.println(docBasicWithImages);
		System.out.println("---------------------------\n");
		
		System.out.println("\n---------WHITELIST - SIMPLETEXT-----------------");
		String docSimpleText = Jsoup.clean(texto, Whitelist.simpleText());
		System.out.println(docSimpleText);
		System.out.println("---------------------------\n");
		
		System.out.println("\n---------WHITELIST - BASIC-----------------");
		String docBasic = Jsoup.clean(texto, Whitelist.basic());
		System.out.println(docBasic);
		System.out.println("---------------------------\n");
		
	}
	
	public void parseWithCustomWhiteList(String texto) {
		System.out.println("\n---------WHITELIST - CUSTOM WHITELIST-----------------");
		
		String docCustomWhiteList = Jsoup.clean(texto, CustomWhiteList.customWhiteList());
		Document docWhiteListDocument = Jsoup.parse(docCustomWhiteList);
		docWhiteListDocument.outputSettings().syntax(Syntax.xml);
		docWhiteListDocument.select("p").last().remove();
		docCustomWhiteList = docWhiteListDocument.toString();
		System.out.println(docWhiteListDocument.select("body").html());
		
		System.out.println("\n---------------------------\n");
		System.out.println(docCustomWhiteList);
		System.out.println("---------------------------\n");
	}
	
	
}
