package org.example;


import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;

public class CustomWhiteList extends Whitelist {

	public CustomWhiteList() {
		super();
	}
	
	public static Whitelist customWhiteList() {
		return relaxed()
				.addAttributes("p", "style", "align")
				.addAttributes("span", "style")
				.addAttributes("table", "class", "style", 
						"border", "cellpadding", "cellspacing")
				.addAttributes("tbody", "style")
				.addAttributes("tr", "style", "width")
				.addAttributes("td", "style", "width")
				.addAttributes("div", "class", "style", "width");
	}
	
	@Override
	protected boolean isSafeAttribute(String tagName, Element el, Attribute attr) {
		return ("p".equals(tagName) && 
				"style".equals(attr.getKey()) && 
				attr.getValue().startsWith("font-size")) || super.isSafeAttribute(tagName, el, attr);
	}
	
}
