package org.example.test;


import org.example.ParseHtmlHelp;
import org.junit.Test;

public class ParseHtmlHelpTest {

	public static String parseHtmlHelpTest() {
		StringBuilder texto = new StringBuilder();
		
		texto.append("<p style='text-align: justify;'>");
		texto.append("<span style='font-size: small; font-family: arial,helvetica,sans-serif;'>");
		texto.append("PORTARIA Nº 122/2014 - GAB/REIT.");
		texto.append("</span>");
		texto.append("</p>");
		
		texto.append("<p align='justify'>");
		texto.append("<span style='font-size: small; font-family: arial,helvetica,sans-serif;'>");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("<p align='justify'>");
		texto.append("<span style='font-size: small; font-family: arial,helvetica,sans-serif;'>");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("RESOLVE");
		texto.append("</span>");
		texto.append("</p>");
		
		texto.append("<p></br></p>");
		
		texto.append("<p align='justify'>");
		texto.append("<span style='font-size: small; font-family: arial,helvetica,sans-serif;'>");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("Designar (o)a servidor(a) RICHARD NIXON DO NASCIMENTO OLIVEIRA, CPF 752.146.873-20, ocupante do cargo de ADMINISTRADOR, matrícula SIAPE n.º 1888079, ");
		texto.append("para exercer a Função Gratificada, FG-02, de Pregoeiro do(a) ");
		texto.append("Comissão Permanente de Licitação, desta Universidade. ");
		texto.append("</span>");
		texto.append("<p></br></p>");
		
		texto.append("<p align='justify'>");
		texto.append("<span style='font-size: small; font-family: arial,helvetica,sans-serif;'>");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("Dê-se ciência. Publique-se. Cumpra-se.");
		texto.append("</span>");
		texto.append("</p>");
		texto.append("<p></br></p>");
		
		texto.append("<p align='justify'>");
		texto.append("<span style='font-size: small; font-family: arial,helvetica,sans-serif;'>");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		texto.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		
		texto.append("<table border='1' cellspacing='0' cellpadding='0'>");
		texto.append("<tbody>");
		
		texto.append("<tr>");
		
		texto.append("<td valign='top' width='191'>");
		texto.append("<p align='center'>");
		texto.append("<strong>CAMPUS</strong>");
		texto.append("</p>");
		texto.append("</td>");
		
		texto.append("<td valign='top' width='244'>");
		texto.append("<p align='center'>");
		texto.append("<strong>CURSO</strong>");
		texto.append("</p>");
		texto.append("</td>");

		texto.append("<td valign='top' width='139'>");
		texto.append("<p align='center'>");
		texto.append("<strong>VAGAS</strong>");
		texto.append("</p>");
		texto.append("</td>");
		
		texto.append("<td valign='top' width='176'>");
		texto.append("<p align='center'>");
		texto.append("<strong>TURNO</strong>");
		texto.append("</p>");
		
		texto.append("</tr>");
		
		texto.append("<tr>");
		
		texto.append("<td width='191'>");
		texto.append("<p align='center'>");
		texto.append("Campus de S&atilde;o Lu&iacute;s");
		texto.append("</p>");
		texto.append("</td>");
		
		texto.append("<td width='244'>");
		texto.append("<p align='center'>");
		texto.append("Engenharia Ambiental e Sanit&aacute;ria");
		texto.append("</p>");
		texto.append("</td>");
		
		texto.append("<td width='139'>");
		texto.append("<p align='center'>");
		texto.append("30");
		texto.append("</p>");
		texto.append("</td>");
		
		texto.append("<td width='176'>");
		texto.append("<p align='center'>");
		texto.append("Matutino e Vespertino");
		texto.append("</p>");
		texto.append("</td>");
		
		texto.append("</tr>");
		
		texto.append("</tbody>");
		texto.append("</table>");
	
		texto.append("<p></br></p>");
		
		texto.append("São Luís (MA), 18 de fevereiro de 2014. &nbsp;");
		texto.append("</span>");
		texto.append("</p>");
		texto.append("<p></br></p>");
		
		texto.append("<p align='center' style='font-size:12pt; font-family: Times New Roman, Georgia, Serif;'>");
		texto.append("(Autenticado em 24/08/2016)");
		texto.append("</br>FERNANDO CARVALHO SILVA</br>VICE-REITOR</br>Matrícula:1086109</p>");
		
		return texto.toString();
	}
	
	@Test
	public void parseWithCustomWhiteListTest() {
		ParseHtmlHelp parseHtmlHelp = new ParseHtmlHelp();
		parseHtmlHelp.parseWithCustomWhiteList(parseHtmlHelpTest());
	}
	
	@Test
	public void parseTest() {
		ParseHtmlHelp parseHtmlHelp = new ParseHtmlHelp();
		parseHtmlHelp.parse(parseHtmlHelpTest());
	}

}
